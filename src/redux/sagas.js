import {apiLogin} from './api';
import {all, takeLatest, put} from 'redux-saga/effects';
// import {getHeaders, saveToken, removeToken} from './config';
import AsyncStorage from '@react-native-async-storage/async-storage';

function* loginSaga(action) {
  try {
    const res = yield apiLogin(action.payload);
    console.log('1');
    
    if (res && res.data) {
      console.log('2');
      AsyncStorage.setItem('token', res.data.token);
      yield put({type: 'loginSuccess'});
    } else {
      yield put({type: 'loginFailed'});
    }
  } catch (e) {
    console.log('3');
    console.info('e', e);
    yield put({type: 'loginFailed'});
  }
}

function* rootSaga() {
  yield takeLatest('login', loginSaga);
}

export default function* saga() {
  yield all([rootSaga()]);
}
