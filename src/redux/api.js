import axios from 'axios';

export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: 'https://reqres.in/api/login',
    data: dataLogin,
  });
}
