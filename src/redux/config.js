import AsyncStorage from '@react-native-async-storage/async-storage';

export async function getHeaders() {
  const token = await AsyncStorage.getItem('token');

  return {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + token,
  };
}

export async function saveToken(token) {
  AsyncStorage.setItem('token', token);
}

export async function removeToken(token) {
  AsyncStorage.removeItem('token');
}
