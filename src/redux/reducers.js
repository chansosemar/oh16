import {combineReducers} from 'redux';

const initialState = {
  name: 'Wildan',
  theme: '',
  isLogin: false,
  isLoading: false,
};

const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'dark': {
      return {
        ...state,
        theme: 'on',
      };
    }
    case 'light': {
      return {
        ...state,
        theme: 'off',
      };
    }
    case 'auto': {
      return {
        ...state,
        theme: '',
      };
    }
    case 'gantiNama': {
      return {
        ...state,
        name: action.payload,
      };
    }
    case 'login': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'loginSuccess': {
      return {
        ...state,
        isLogin: true,
        isLoading: false,
      };
    }
    case 'loginFailed': {
      return {
        ...state,
        isLogin: false,
        isLoading: false,
      };
    }
    case 'logout': {
      return {
        ...state,
        isLogin: false,
      };
    }

    default:
      return state;
  }
};

export default combineReducers({
  themeReducer,
});
