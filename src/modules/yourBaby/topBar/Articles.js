import React from 'react';
import {Text, View} from 'react-native';
import Card from './Card';

const Articles = () => {
  const listData = ['chandra', 'wildan', 'kevin'];

  return (
    <View>
      {listData.map((e, i) => (
        <>
          <Card title={e} type={e} />
        </>
      ))}
    </View>
  );
};

export default Articles;
