import React from 'react';
import {Text, View} from 'react-native';

const Card = ({title, type}) => {
  return (
    <View
      style={{
        backgroundColor: type === 'chandra' ? 'green' : 'red',
        padding: 20,
        margin: 10,
      }}>
      <Text>{title}</Text>
    </View>
  );
};
const Kartu = ({title, type}) => {
  return (
    <View
      style={{
        backgroundColor: type === 'chandra' ? 'blue' : 'red',
        padding: 20,
        margin: 10,
      }}>
      <Text>{title}</Text>
    </View>
  );
};

export default Card;
