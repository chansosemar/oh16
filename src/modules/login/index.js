import React, {useState} from 'react';
import {View, Text, TextInput, Button, ActivityIndicator} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

const Login = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.themeReducer.isLoading);
  const [email, setEmail] = useState('eve.holt@reqres.in');
  const [password, setPassword] = useState('cityslicka');

  const submitLogin = () => {
    dispatch({
      type: 'login',
      payload: {
        email,
        password,
      },
    });
  };

  return (
    <View>
      <TextInput
        style={{backgroundColor: 'red', color: 'white'}}
        placeholder="isi nama"
        label="Name"
        value={email}
        onChangeText={text => setEmail(text)}
      />
      <TextInput
        style={{backgroundColor: 'red', color: 'white'}}
        placeholder="isi password"
        label="Password"
        value={password}
        onChangeText={text => setPassword(text)}
      />
      <Button title="Login" onPress={submitLogin} />

      {isLoading && <ActivityIndicator size="large" color="black" />}
    </View>
  );
};

export default Login;
